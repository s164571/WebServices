package test.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StringCalculator {

	//Task 1
	public static int addWithCommaAndMaxTwo(String numbers) {
		int result = 0;
	    String[] nums = numbers.split(",");
	    int[] answer = new int[nums.length];
	    
	    if (nums.length > 2) {
	        throw new RuntimeException("More than 2 numbers");
	    }
	    for (int i = 0; i < nums.length; i++) {
			answer[i] = Integer.parseInt(nums[i]);
			result += answer[i];
		}
		return result;
    }
	
	//Task 2
	public static int addWithCommaAnyNumber(String numbers) {
		int result = 0;
		String[] nums = numbers.split(",");
		int[] answer = new int[nums.length];
		
		for (int i = 0; i < nums.length; i++) {
			answer[i] = Integer.parseInt(nums[i]);
			result += answer[i];
		}
		return result;
    }
	

	//Task 4
	public static int add(String numbers) {
		String delimiter = ",|n"; //Initial delimiters
		if (numbers.startsWith("//")) {
			int delimiterIndex = numbers.indexOf("//") + 2; //The first delimiter is found
			delimiter = numbers.substring(delimiterIndex, delimiterIndex + 1); //Pick next delimiter
			numbers = numbers.substring(numbers.indexOf("n") + 1); //After a new line it is a number
		}
		return addNegative(numbers, delimiter);
	}

	//Task 3 and 5
	public static int addNegative(String numbers, String delimiter) {
		int result = 0;
		String[] nums = numbers.split(delimiter);
		ArrayList<Integer> negativeNums = new ArrayList<Integer>();
		int[] answer = new int[nums.length];

		if (numbers.contains(",n") || numbers.contains("n,")) {
			throw new RuntimeException("Wrong sequence");
		}
		else {
			for (int i = 0; i < nums.length; i++) {
				answer[i] = Integer.parseInt(nums[i]);
				if (answer[i] < 0) {
					negativeNums.add(answer[i]);
	            }
				result += answer[i];
			}
		}
		if (negativeNums.size() > 0) {
			throw new RuntimeException("Negatives are not allowed" + negativeNums);
	    } 
		return result;
    }
	
	public static void main(String[] args) throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String numbers = input.readLine();
		System.out.println(StringCalculator.add(numbers));
	}

}