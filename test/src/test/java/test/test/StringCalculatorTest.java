package test.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class StringCalculatorTest extends TestCase {

	private StringCalculator strCalculator;

	@Before
	public void setUp() {
		strCalculator = new StringCalculator();
    }
	
    @Test
    public void testOneNumber() {
        assertEquals(3, StringCalculator.addWithCommaAndMaxTwo("3"));
    }

    @Test
    public void testTwoNumbers() {
        assertEquals(7, StringCalculator.addWithCommaAndMaxTwo("4,3"));
    }

    @Test
    public void testThreeNumbersTrue() {
        assertEquals(17, StringCalculator.addWithCommaAnyNumber("4,3,10"));
    }
    
    @Test(expected = RuntimeException.class)
    public void testThreeNumbersFalse() {
        StringCalculator.addWithCommaAndMaxTwo("4,3,10");
    }

    @Test
    public void testNewlineDelim() {
        assertEquals(7, StringCalculator.addWithCommaAndMaxTwo("4n3"));
    }

    @Test
    public void testMixedDelimTrue() {
        assertEquals(17, StringCalculator.add("4,3n10"));
    }
    
    @Test(expected = RuntimeException.class)
    public void testMixedDelimFalse1() {
        assertEquals(17, StringCalculator.add("4,n10"));
    }
    
    @Test(expected = RuntimeException.class)
    public void testMixedDelimFalse2() {
        assertEquals(17, StringCalculator.add("4n,10"));
    }

    @Test
    public void testCustomDelim() {
        assertEquals(3, StringCalculator.add("//;\n1;2"));
    }

    @Test
    public void testNegativeOneNumber() {
        Boolean threwException = false;
        try {
        	StringCalculator.add("-3");
        }
        catch(IllegalArgumentException ex) {
            threwException = true;
            //out.print("\nException message for testNegativeOneNumber: " + ex.getMessage() + "\n");
        }
        assertTrue(threwException);
    }

    @Test
    public void testNegativeTwoNumbers() {
        Boolean threwException = false;
        try {
        	StringCalculator.add("-4,-3");
        }
        catch(IllegalArgumentException ex) {
            threwException = true;
            //out.print("\nException message for testNegativeTwoNumbers: " + ex.getMessage() + "\n");
        }
        assertTrue(threwException);
    }

    @Test
    public void testNegativeNumbersWithCustomDelim() {
        Boolean threwException = false;
        try {
        	StringCalculator.add("//;\n1;-2");
        }
        catch(IllegalArgumentException ex) {
            threwException = true;
            //out.print("\nException message for testNegativeNumbersWithCustomDelim: " + ex.getMessage() + "\n");
        }
        assertTrue(threwException);
    }


    @Test
    public void testNegSignAsDelimiter() {
        Boolean threwException = false;
        try {
        	StringCalculator.add("//-\n5-3");
        }
        catch(IllegalArgumentException ex) {
            threwException = true;
            //out.print("\nException message for testNegSignAsDelimiter: " + ex.getMessage() + "\n");
        }
        assertTrue(threwException);
    }

    @Test
    public void test1000() {
        assertEquals(1006, StringCalculator.add("1000,6"));
    }
}