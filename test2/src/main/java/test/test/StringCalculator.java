package test.test;

import java.lang.Number;

public class StringCalculator {

	private int numberArray[];
	private int sum = 0;
	
	
	public StringCalculator() {
		
	}
	
	public int add(String numbers) {
		if (numbers.isEmpty()) {
			return 0;
		}
		
		if (numbers.length() == 1) {
			return Integer.parseInt(numbers);
		}
		
		if (numbers.length() > 1) {
			String numbers1[] = numbers.split(",");
			for (String number : numbers1) {  // loop through all the number in the string array
			    Integer n = Integer.parseInt(number);  // parse each number
			    sum += n;     // sum the numbers
			    System.out.println(sum); 
			  }
		return sum;
		}
		return -1;
		}
	
}
