package test.test;

import static org.junit.Assert.*;
 // Allows you to use directly assert methods, such as assertTrue(...), assertNull(...)
import org.junit.Test; // for @Test

import org.junit.Before; // for @Before

public class StringCalculatorTest {
	 
	private StringCalculator stringCalculator;
	
	@Before
	public void setUp() {
        stringCalculator = new StringCalculator();
    }
	
	@Test
    public void should_return_0_when_passed_string_is_empty() {
        assertEquals(stringCalculator.add(""), 0);
    }

	@Test
	public void should_return_the_sum_of_number_passed_as_string() {
		assertEquals(stringCalculator.add("1"), 1);
	}
	
	@Test
	public void should_return_the_sum_of_numbers_passed_as_string() {
		assertEquals(stringCalculator.add("2,3"), 5);
	}
	
	

}
	
