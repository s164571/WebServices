package test.test;

import java.util.Arrays;
import java.util.Iterator;

public class StringCalculator {

    public static void main( String[] args )
    {
    	String numbers2 = "1,";
    	String numbers4 = "1\n";
    	String numbers3 = "//;\n1;2";
    	String numbers5 = "//;\n1;2;-3;3;-4";
    	String numbers = "1\n,1,1";
        System.out.println(add(numbers3));
    }
	
	public static int add(String numbers) {		
		if(numbers.isEmpty()) {
			return (int) 0f;
		}
		
		char splitter = ',';
		if(numbers.startsWith("//") && numbers.startsWith("\n", 3)) {
			splitter = numbers.charAt(2);
			numbers = numbers.substring(4);
		}
		
		String[] numbersList = numbers.split(Character.toString(splitter));

		if (numbers.contains("-")) {
			String expection = "negatives not allowed :";
			for (String s : numbersList) {
				if(s.contains("-")) {
					expection = expection + " " + s;
				}
			}
			throw new IllegalArgumentException(expection);
		}
		
		if(numbersList.length == 1) {
			return Integer.parseInt(numbersList[0]);
		}
		else{
			int returnValue= (int) 0.0f;
			for(String s : numbersList) {
				s = s.replaceAll("\n", "");
				returnValue += Integer.parseInt(s);
			}
			return returnValue;
		}
	}

}
