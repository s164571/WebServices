package test.test;

import static org.junit.Assert.*; // Allows you to use directly assert methods, such as assertTrue(...), assertNull(...)

import org.junit.Test; // for @Test

import junit.framework.Assert;

import org.junit.Before; // for @Before

public class StringCalculatorTest {
	 
	private StringCalculator stringCalculator;
	
	@Before
	public void setUp() {
        stringCalculator = new StringCalculator();
    }
	
	@Test
    public void should_return_0_when_passed_string_is_empty() {
        assertEquals(stringCalculator.add(""), 0);
    }
	
	@Test
	public void should_return_single_entry() {
		assertEquals(stringCalculator.add("2"), 2);
	}
	
	@Test
	public void should_return_the_sum_of_numbers_passed_as_string() {
		assertEquals(stringCalculator.add("2,3"), 5);
	}
	@Test
	public void test1() {
		assertEquals(stringCalculator.add("2\n,3,1,1,1,1"), 9);
	}
	@Test
	public void test2() {
		assertEquals(stringCalculator.add("//;\n1;2"), 3);
	}
	@Test
	public void test3() {
		assertEquals(stringCalculator.add("//;\n1;2;2;2"), 7);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test4() {	
		stringCalculator.add("//;\n1;2;-3;3;-4");
	}   
	
	/*
	@Test
	public void test5() {	
	    try {
	    	stringCalculator.add("//;\n1;2;-3;3;-4");
	        fail(" negatives not allowed : -3 -4");
	    } catch(IndexOutOfBoundsException e) {
	    }
	}    
	*/
	   
}
	
